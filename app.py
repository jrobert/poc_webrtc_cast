from flask import Flask, request
from flask_socketio import SocketIO, join_room, leave_room

app = Flask(__name__)
sio = SocketIO(app)

connexions_actives={}

@sio.event
def connect():
    print('Connected', request.sid)

@sio.event
def disconnect():
    try:
        quidonc = next(x for x in connexions_actives if connexions_actives[x]["id"]==request.sid)
        leave_room(connexions_actives[quidonc]["room"])
        del data[quidonc]
        sio.emit('disconnect', room=connexions_actives[quidonc]["room"] )
    except StopIteration as e:
       pass

@sio.event
def server(room):
    join_room(room)
    connexions_actives["serveur"] = {"room": room, "id": request.sid}
    if "client" in connexions_actives and connexions_actives["client"]["room"] == room:
        sio.emit('ready', room=room, skip_sid=request.sid   )

@sio.event
def client(room):
    connexions_actives["client"] = {"room": room, "id": request.sid}
    join_room(room)
    if "serveur" in connexions_actives and connexions_actives["serveur"]["room"] == room:
        sio.emit('ready', room=room, skip_sid=request.sid   )

@sio.event
def candidate(data):
    sio.emit('candidate', data, skip_sid=request.sid)


@sio.event
def data(data):
    sio.emit('data', data, skip_sid=request.sid)

@app.route("/")
def main():
    return "Bonjour"