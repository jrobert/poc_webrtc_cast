const SIGNALING_SERVER_URL = '' //'http://localhost:5000';
// const TURN_SERVER_URL = 'localhost:3478';
// const TURN_SERVER_USERNAME = 'username';
// const TURN_SERVER_CREDENTIAL = 'credential';
// WebRTC config: you don't have to change this for the example to work
// If you are testing on localhost, you can just use PC_CONFIG = {}
const PC_CONFIG = {
  // iceServers: [
  //   {
  //     urls: 'turn:' + TURN_SERVER_URL + '?transport=tcp',
  //     username: TURN_SERVER_USERNAME,
  //     credential: TURN_SERVER_CREDENTIAL
  //   },
  //   {
  //     urls: 'turn:' + TURN_SERVER_URL + '?transport=udp',
  //     username: TURN_SERVER_USERNAME,
  //     credential: TURN_SERVER_CREDENTIAL
  //   }
  // ]
};

// Signaling methods
let socket = io(SIGNALING_SERVER_URL, { autoConnect: false });

socket.on('data', (data) => {
  handleSignalingData(data);
});


let handleSignalingData = (data) => {
  console.log("DATA", data);
  switch (data.type) {
    case 'offer':
      createPeerConnection();
      pc.setRemoteDescription(new RTCSessionDescription(data));
      sendAnswer();
      break;
    case 'answer':
      pc.setRemoteDescription(new RTCSessionDescription(data));
      break;
    case 'candidate':
      pc.addIceCandidate(new RTCIceCandidate(data.candidate));
      break;
  }
};

socket.on('ready', () => {
  createPeerConnection();
  sendOffer();
});

let sendData = (data) => {
  socket.emit('data', data);
};

// WebRTC methods
let pc=null;
let localStream;
let remoteStreamElement = document.querySelector('#remoteStream');


let createPeerConnection = () => {
  try {
    pc = new RTCPeerConnection(PC_CONFIG);
    pc.onicecandidate = onIceCandidate;
    pc.ontrack = onTrack;
    pc.oniceconnectionstatechange=changementEtatConnexion;

    console.log('PeerConnection created');
  } catch (error) {
    console.error('PeerConnection failed: ', error);
  }
};



function changementEtatConnexion (){
  if(pc.iceConnectionState == "failed"){
    remoteStreamElement.srcObject = null;
  }
}

let sendOffer = () => {
  pc.createOffer({
    offerToReceiveAudio: 1,
    offerToReceiveVideo: 1
    }).then(
    setAndSendLocalDescription,
    (error) => { console.error('Send offer failed: ', error); }
  );
};

let sendAnswer = () => {
  pc.createAnswer().then(
    setAndSendLocalDescription,
    (error) => { console.error('Send answer failed: ', error); }
  );
};

let setAndSendLocalDescription = (sessionDescription) => {
  pc.setLocalDescription(sessionDescription);
  sendData(sessionDescription);
};

let onIceCandidate = (event) => {
  if (event.candidate) {
    sendData({
      type: 'candidate',
      candidate: event.candidate
    });
  }
};

let onTrack = (event) => {
  remoteStreamElement.srcObject = event.streams[0];
};


room =  String(Math.floor(Math.random() * 10000));
socket.connect();
socket.emit('server',room);

let mdp = document.getElementById("mdp");
mdp.textContent = room;

