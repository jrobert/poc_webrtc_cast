## Desciption

Serveur web permettant la retransmission d'un écran/une fenêtre depuis une machine distante via le navigateur.

On appelle "client" la machine qui souhaite retransmettre du contenu, et "serveur" la machine sur laquelle on souhaite voir s'afficher ce contenu.

Sur le serveur, lancer le code : 

virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
flask run


Sur le serveur, dans un navigateur, aller à l'addresse : http://IPSERVEUR/static/serveur.html

Sur la machine client, aller à l'addresse : http://IPSERVEUR/static/client.html


